let dataArray = [];

get = function (url) {
    fetch(url)
        .then(response => response.json())
        .then(function (data) {
            all = data.people;
            for (let id = 0; id < all.length; id++) {
                let pos = all[id].contact.location.lon.toString() + " - " + +all[id].contact.location.lat.toString();
                dataArray.push({
                    id: all[id].id,
                    firstname: all[id].firstname,
                    lastname: all[id].lastname,
                    gender: all[id].gender,
                    email: all[id].contact.email,
                    address: all[id].contact.address,
                    city: all[id].contact.city,
                    country: all[id].contact.country,
                    pos,
                    phone: all[id].contact.phone,
                    pet: all[id].preferences.favorite_pet,
                    fruit: all[id].preferences.favorite_fruit,
                    color: all[id].preferences.favorite_color,
                    movie: all[id].preferences.favorite_movie
                });
            }
            createTable();
        });
};
get('https://run.mocky.io/v3/70e5b0ad-7112-41c5-853e-b382a39e65b7');


createTable = function () {
    table = new Tabulator("#people-table", {
        data: dataArray, //load row data from array
        layout: "fitColumns", //fit columns to width of table
        responsiveLayout: "hide", //hide columns that dont fit on the table
        tooltips: true, //show tool tips on cells
        addRowPos: "top", //when adding a new row, add it to the top of the table
        history: true, //allow undo and redo actions on the table
        pagination: "local", //paginate the data
        paginationSize: 20, //allow 7 rows per page of data
        movableColumns: false, //allow column order to be changed
        resizableRows: true, //allow row order to be changed
        initialSort: [ //set the initial sort order of the data
            { column: "id", dir: "asc" },
        ],
        columns: [ //define the table columns
            { title: "ID", field: "id", editor: "input", validator: "required", headerFilter: "input" },
            { title: "First Name", field: "firstname", editor: "input", validator: "required", headerFilter: "input" },
            { title: "Last Name", field: "lastname", editor: "input", validator: "required", headerFilter: "input" },
            { title: "Gender", field: "gender", width: 95, editor: "select", editorParams: { values: ["male", "female"] }, headerFilter: true, headerFilterParams: { "male": "Male", "female": "Female" } },
            { title: "E-Mail", field: "email", editor: "input", validator: "required", headerFilter: "input" },
            { title: "Address", field: "address", editor: "input", validator: "required", headerFilter: "input" },
            { title: "City", field: "city", editor: "input", validator: "required", headerFilter: "input" },
            { title: "Country", field: "country", editor: "input", validator: "required", headerFilter: "input" },
            { title: "Position", field: "pos", editor: "input", validator: "required", headerFilter: "input" },
            { title: "Phone", field: "phone", editor: "input", validator: "required", headerFilter: "input" },
            { title: "Favorite Pet", field: "pet", editor: "input", validator: "required", headerFilter: "input" },
            { title: "Favorite Fruit", field: "fruit", editor: "input", validator: "required", headerFilter: "input" },
            { title: "Favorite Color", field: "color", editor: "input", validator: "required", headerFilter: "input" },
            { title: "Favorite Movie", field: "movie", editor: "input", validator: "required", headerFilter: "input" },
        ],
    });

    //trigger download of data.json file
    document.getElementById("download-json").addEventListener("click", function () {
        table.download("json", "people.json");
    });
};

