let allGenders = [];
let countGenders = [];
let allCountries = [];
let countries = [];
let allColors = [];
let countColors = [];
get = function (url) {
    fetch(url)
        .then(response => response.json())
        .then(function (data) {
            all = data.people;
            console.log(all);
            // gender charts
            for (let id = 0; id < all.length; id++) {
                if (allGenders.includes(all[id].gender) == false)
                    allGenders.push(all[id].gender);
            }

            for (let id = 0; id < allGenders.length; id++) {
                countGenders.push(all.filter(x => x.gender == allGenders[id]).length);
            }
            genderCharts();
            // country charts
            for (let id = 0; id < all.length; id++) {
                if (allCountries.includes(all[id].contact.country) == false) {
                    allCountries.push(all[id].contact.country)
                }
            }
            for (let id = 0; id < allCountries.length; id++) {
                countries.push(all.filter(x => x.contact.country == allCountries[id]).length)
            }
            countryCharts();
            // color charts
            for (let id = 0; id < all.length; id++) {
                if (allColors.includes(all[id].preferences.favorite_color) == false)
                    allColors.push(all[id].preferences.favorite_color);
            }

            for (let id = 0; id < allColors.length; id++) {
                countColors.push(all.filter(x => x.preferences.favorite_color == allColors[id]).length);
            }
            colorCharts();
        });


};
get('https://run.mocky.io/v3/70e5b0ad-7112-41c5-853e-b382a39e65b7');


let randomcolors = [];

function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
};

function randomColors(size) {
    randomcolors = [];
    for (let i = 0; i < size; i++) {
        randomcolors.push('rgba(' + getRandomInt(255) + ',' + getRandomInt(255) + ',' + getRandomInt(255) + ', 0.2)')
    }
    return randomcolors;
};



var genderCharts = function () {
    var ctx = document.getElementById('gender');
    var myDoughnutChart = new Chart(ctx, {
        type: 'doughnut',
        data: {
            labels: allGenders,
            datasets: [{
                data: countGenders,
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                ],
                borderWidth: 1
            }]
        }
    });
}

var countryCharts = function () {
    randomColors(countries.length);
    var ctx = document.getElementById('country');
    var myDoughnutChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: allCountries,
            datasets: [{
                data: countries,
                backgroundColor: randomcolors,
                borderWidth: 1
            }]
        }
    });
}

var colorCharts = function () {
    randomColors(countries.length);
    var ctx = document.getElementById('colors');
    var myDoughnutChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: allColors,
            datasets: [{
                data: countColors,
                backgroundColor: randomcolors,
                borderWidth: 1
            }]
        }
    });
}